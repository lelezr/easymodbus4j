package com.github.zengfr.easymodbus4j.app.common;

public class DeviceArg {
   public String deviceId;
   public String ip;
   public int port;
   public String version;
}
